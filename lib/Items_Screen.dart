import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/Cate_Maps.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/Shop_Name.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Models/Market/GetMarketsByCategory.dart';
import 'localization/DemoLocalization.dart';

class ItemsScreen extends StatefulWidget {
  final double lat;
  final double lng;
  final int cateId;
  final String name;

  const ItemsScreen({Key key, this.lat, this.lng, this.cateId, this.name})
      : super(key: key);
  @override
  _ItemsScreenState createState() => _ItemsScreenState();
}

class _ItemsScreenState extends State<ItemsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  void showLoadingDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text(
                      "${DemoLocalizations.of(context).trans('wait')}",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  double rating = 0;
  int starCount = 5;
  bool _isLoading = false;
  var connectivityResult;
  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  @override
  void initState() {
    _isLoading = true;
    _submit(context);
    connect();
    print(widget.lat.toString() + '    =>>>>>>>>>>>>> lat');
    print(widget.lng.toString() + '    =>>>>>>>>>>>>> lng');
    super.initState();
  }

  GetMarketsByCategory _byCategory;
  NetworkUtil _util = NetworkUtil();
  _submit(BuildContext context) async {
    FormData _formData = FormData.from(
        {"category_id": widget.cateId, "lat": widget.lat, "long": widget.lng});
    SharedPreferences _prfes = await SharedPreferences.getInstance();
    FormData _headers = FormData.from(
        {"Content-Type": "application/json", "lang": _prfes.getString('lang')});

    _util
        .post('getMarketsByCategoryId', body: _formData, headers: _headers)
        .then((result) async {
      if (result.statusCode == 200) {
        setState(() {
          _byCategory = GetMarketsByCategory.fromJson(result.data);

          _isLoading = false;
        });
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              print(result.data['message']);
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                title: Text(result.data['message']),
                actions: <Widget>[
                  Center(
                    child: FlatButton(
                      child:
                          Text('${DemoLocalizations.of(context).trans('es')}'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              );
            });
      }
    }).catchError((err) {
      print("333333333333333333333333#########" + err.toString());
    });
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: InkWell(
            onTap: () {
              
            },
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: "Cairo",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return
    
    Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.height,
      child: _isLoading
                ? Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                  )
                :
       Directionality(
        textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
            ? TextDirection.ltr
            : TextDirection.rtl,
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text(
                widget.name,
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
              backgroundColor: Theme.of(context).primaryColor,
              leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios),
              ),
            ),
            floatingActionButton:
            
            _byCategory.data.length > 0 ?
             FloatingActionButton(
                backgroundColor: Colors.white.withOpacity(0),
                onPressed: () async {
                  var connectivityResult =
                      await Connectivity().checkConnectivity();
                  if (connectivityResult == ConnectivityResult.mobile ||
                      connectivityResult == ConnectivityResult.wifi) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CateMaps(
                                  cateID: widget.cateId,
                                  lat: widget.lat,
                                  lang: widget.lng,
                                )));
                  } else {
                    showInSnackBar(
                        '${DemoLocalizations.of(context).trans('no_internet')}');
                  }
                },
                child: Image(
                  image: AssetImage(
                    'assets/flip.png',
                  ),
                  height: 120,
                  alignment: Alignment.center,
                  fit: BoxFit.cover,
                )):null,
            body: _isLoading
                ? Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                  )
                : Container(
                    child: _buildItem(),
                  ),
                  

                  ),
      ),
    );
  }

  Widget _buildItem() {
    return _byCategory.data.length > 0
        ? ListView.builder(
            shrinkWrap: true,
            itemCount: _byCategory.data.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () async {
                  var connectivityResult =
                      await Connectivity().checkConnectivity();
                  if (connectivityResult == ConnectivityResult.mobile ||
                      connectivityResult == ConnectivityResult.wifi) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShopName(
                              

                                  phone: _byCategory.data[index].phone,

                                  distance :_byCategory.data[index].distance,
                                  
                                  id: _byCategory.data[index].id,
                                  
                                  name: _byCategory.data[index].name,
                                  
                                  start: _byCategory.data[index].startTime,
                                  
                                  end: _byCategory.data[index].endTime,
                                  
                                  image: _byCategory.data[index].image,

                                  long: double.parse(_byCategory.data[index].longitude),

                                  lat:  double.parse(_byCategory.data[index].latitude,
                                  
                                  
                                  
                                  ),
                                )));
                  } else {
                    showInSnackBar(
                        '${DemoLocalizations.of(context).trans('no_internet')}');
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 10, left: 10, top: 10),
                  child: Container(
                    height: 120,
                    width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(.2)),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 100,
                              width: 100,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(right: 10, left: 10),
                                child: Container(
                                  width: 90,
                                  height: 130,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child: FadeInImage.assetNetwork(
                                      placeholder: 'assets/logo.png',
                                      image: _byCategory.data[index].image,
                                      height: 100,
                                      width: MediaQuery.of(context).size.width,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              height: 130,
                              width: MediaQuery.of(context).size.width - 143,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          _byCategory.data[index].name,
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                right: 3, left: 3),
                                            child: StarRating(
                                              size: 17.0,
                                              rating:
                                                  _byCategory.data[index].rate,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              borderColor: Theme.of(context)
                                                  .primaryColor,
                                              starCount: starCount,
                                            )),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                              'assets/time.png',
                                            ),
                                            height: 15,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            " ${DemoLocalizations.of(context).trans('fro')}  " +
                                                _byCategory
                                                    .data[index].startTime +
                                                "   " +
                                                " ${DemoLocalizations.of(context).trans('t')}  " +
                                                _byCategory
                                                    .data[index].endTime +
                                                "   ",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                              'assets/location.png',
                                            ),
                                            height: 17,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            '${DemoLocalizations.of(context).trans('distance')}  "${_byCategory.data[index].distance}" ${DemoLocalizations.of(context).trans('km')}',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        )),
                  ),
                ),
              );
            },
          )
        : Center(
            child: Text("${DemoLocalizations.of(context).trans('no_data')}",
            style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 19),
            ),
          );
  }
}
