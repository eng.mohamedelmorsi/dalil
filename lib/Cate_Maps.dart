import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/Shop_Name.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:location/location.dart';
import 'package:geolocator/geolocator.dart';
import 'Models/Market/GetMarketsByCategory.dart';
import 'localization/DemoLocalization.dart';

class CateMaps extends StatefulWidget {
  final int cateID, distance;
  final String image, phone, end, start, name;
  final double lat;
  final double lang;
  const CateMaps(
      {Key key,
      this.cateID,
      this.image,
      this.phone,
      this.end,
      this.start,
      this.lat,
      this.lang,
      this.distance,
      this.name})
      : super(key: key);

  @override
  _CateMapsState createState() => _CateMapsState();
}

class _CateMapsState extends State<CateMaps> {
  BitmapDescriptor myIcon;
  GetMarketsByCategory _byCategory;
  NetworkUtil _util = NetworkUtil();

  GoogleMapController myController;
  double currentLat;
  double currentLong;
  dynamic currentAddress;
  String searchAddress = 'Search with Locations';
  Position updatedPosition;
  Marker marker = Marker(
    markerId: MarkerId("1"),
  );
  Set<Marker> mark = Set();
  var location = Location();
  List<Marker> _markers = [];

  _submit(BuildContext context) async {
    FormData _formData = FormData.from(
        {"category_id": widget.cateID, "lat": widget.lat, "long": widget.lang});

    FormData _headers = FormData.from({
      "Content-Type": "application/json",
    });

    _util
        .post('getMarketsByCategoryId', body: _formData, headers: _headers)
        .then((result) async {
      _byCategory = GetMarketsByCategory.fromJson(result.data);
      if (result.statusCode == 200) {
        setState(() {
          for (int i = 0; i < _byCategory.data.length; i++) {
            print(_byCategory.data.length.toString() + '      assdfsdfsdf');
            _markers.add(Marker(
                markerId: MarkerId(
                  _byCategory.data[i].id.toString(),
                ),
                draggable: false,
                icon: myIcon,
                infoWindow: InfoWindow(title: _byCategory.data[i].name ?? ""),
                position: LatLng(
                  double.parse(_byCategory.data[i].latitude),
                  double.parse(_byCategory.data[i].longitude),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ShopName(
                                lat: double.parse(_byCategory.data[i].latitude),
                                long:
                                    double.parse(_byCategory.data[i].longitude),
                                distance: _byCategory.data[i].distance,
                                phone: _byCategory.data[i].phone,
                                id: _byCategory.data[i].id,
                                name: _byCategory.data[i].name,
                                start: _byCategory.data[i].startTime,
                                end: _byCategory.data[i].endTime,
                                image: _byCategory.data[i].image,
                              )));
                }));
          }
        });
        _byCategory = GetMarketsByCategory.fromJson(result.data);
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              print(result.data['message']);
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                title: Text(result.data['message']),
                actions: <Widget>[
                  Center(
                    child: FlatButton(
                      child: Text('موافق'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              );
            });
      }
    }).catchError((err) {
      print("333333333333333333333333#########" + err.toString());
    });
  }

  void initState() {
    _submit(context);
    BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
        size: Size(
          20,
          20,
        ),
        devicePixelRatio: .9,
      ),
      'assets/blue.png',
    ).then((onValue) {
      myIcon = onValue;
    });

    super.initState();
    LocationPermissions().requestPermissions().then((PermissionStatus status) {
      if (status == PermissionStatus.granted) {
        location.getLocation().then((LocationData myLocation) {
          setState(() {
            currentLat = myLocation.latitude;
            currentLong = myLocation.longitude;

            InfoWindow infoWindow = InfoWindow(title: "Location");
            Marker marker = Marker(
              draggable: true,
              markerId: MarkerId('markers.length.toString()'),
              infoWindow: infoWindow,
              position: LatLng(myLocation.latitude, myLocation.longitude),
              // icon: myIcon,
            );
            setState(() {
              mark.add(marker);
            });
          });
        });
      } else {}
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      myController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("تحديد الموقع"),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: currentLat == null && currentLong == null
            ? Center(
                child: CupertinoActivityIndicator(
                animating: true,
                radius: 15,
              ))
            : Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    GoogleMap(
                      markers: Set.from(_markers),
                      onCameraIdle: () {
                        print(updatedPosition);
                      },
                      onTap: (newLatLong) {
                        currentLat = newLatLong.latitude;
                        currentLong = newLatLong.longitude;
                        print('LAT    ' + currentLat.toString());
                        print('LONG    ' + currentLong.toString());
                        Geolocator()
                            .placemarkFromCoordinates(currentLat, currentLong)
                            .then((address) {
                          setState(() {
                            currentAddress =
                                address[0].name + " " + address[0].country;
                            print("================== $currentAddress");
                          });
                        });
                      },
                      initialCameraPosition: CameraPosition(
                          target: LatLng(currentLat, currentLong), zoom: 15.0),
                      onMapCreated: _onMapCreated,
                      onCameraMove: ((_position) => _updatePosition(_position)),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage('assets/pinkmarker.png'),
                        height: 55,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  void _updatePosition(CameraPosition _position) {
    Position newMarkerPosition = Position(
        latitude: _position.target.latitude,
        longitude: _position.target.longitude);

    setState(() {
      updatedPosition = newMarkerPosition;
      marker = marker.copyWith(
          positionParam:
              LatLng(newMarkerPosition.latitude, newMarkerPosition.longitude));
    });
  }

  searchAndNavigate() {
    Geolocator().placemarkFromAddress(searchAddress).then((result) {
      myController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target:
              LatLng(result[0].position.latitude, result[0].position.longitude),
          zoom: 10.0)));
    });
  }
}
