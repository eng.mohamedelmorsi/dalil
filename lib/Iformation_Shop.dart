import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/MapScreen.dart';
import 'package:delivery_guide/Models/Market/GetALLwa.dart';
import 'package:delivery_guide/Models/Market/setNewMarket.dart';
import 'package:delivery_guide/Models/geAllCities.dart';
import 'package:delivery_guide/Payment.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InformationShop extends StatefulWidget {
  final int cateid;

  const InformationShop({Key key, this.cateid}) : super(key: key);
  @override
  _InformationShopState createState() => _InformationShopState();
}

GlobalKey<FormState> _formKey = GlobalKey<FormState>();

final _scaffoldKey = GlobalKey<ScaffoldState>();

final ScrollController actionScrollController = ScrollController();

class _InformationShopState extends State<InformationShop> {
  NetworkUtil _util = NetworkUtil();
  bool _isLoading = false;
  Datum cit;
  File _imageFile;
  String address;
  bool rememberMe = false;
  bool _autoValidate = false;
  SetNewMarket setNewMarket;
  String name, phone, delegate, state;
  double lat;
  double lang;
  var connectivityResult;
GetAllWaysData getAllWaysData;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _delegateController = TextEditingController();
  TextEditingController _stateController = TextEditingController();
  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: InkWell(
            onTap: () {},
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: "Cairo",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  _submit(BuildContext context) async {
    // _formKey.currentState.save();
    FormState formState = _formKey.currentState;

    void showLoadingDialog() {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              elevation: 10,
              content: Container(
                height: 150,
                width: 150,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Text(
                        "${DemoLocalizations.of(context).trans('wait')}",
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    }

    if (formState.validate()) {
      formState.save();

      FormData _formData = FormData.from({
        "image": _imageFile != null
            ? new UploadFileInfo(new File(_imageFile.path),
                _imageFile.path.substring(_imageFile.path.lastIndexOf("/")))
            : null,
        "name": name,
        "latitude": lat,
        "longitude": lang,
        "city_id": cit.id,
        "category_id": widget.cateid,
        "start_time": startDate1.format(context).trim().toString(),
        "end_time": startDate2.format(context).trim().toString(),
        "delegate_identity": delegate,
        "phone": phone,
        "state": state,
        "way_id":getAllWaysData.id
      });

      FormData _headers = FormData.from({
        "Content-Type": "application/json",
      });
      print(name);
      print(startDate1.format(context).trim().toString());
      print(startDate2.format(context).trim().toString());
      print(cit.id);
      print(widget.cateid);
      print(delegate);
      print(lat.toString() + "         asdasdasdasdasdasd");
      print(lang.toString() + "        asdasdlkklkllklklkllkkasdasdasdasd");
      showLoadingDialog();
      _util
          .post('setNewMarket', body: _formData, headers: _headers)
          .then((result) {
        Navigator.pop(context);
        if (result.statusCode == 200) {
          setNewMarket = SetNewMarket.fromJson(result.data);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Payment()));
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                print(result.data['message']);
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  title: Text(
                    result.data['message'],
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).primaryColor),
                  ),
                  actions: <Widget>[
                    Center(
                      child: FlatButton(
                        child: Text(
                            '${DemoLocalizations.of(context).trans('es')}'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    )
                  ],
                );
              });
        }
      }).catchError((err) {
        print("333333333333333333333333#########" + err.toString());
      });
    }
  }

//--------------getAllCitiesServices---------------------------
  GetAllCities getAllCities = GetAllCities();
  _getAllCities() async {
    SharedPreferences _prfes = await SharedPreferences.getInstance();

    setState(() {
      _isLoading = true;
    });
    FormData _headers = FormData.from({
      "lang": _prfes.getString('lang'),
    });
    Response response = await _util.get('getAllCities', headers: _headers);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        getAllCities = GetAllCities.fromJson(json.decode(response.toString()));
        _isLoading = false;
      });
    } else {}
  }
//---------------------------getWay---------------------
GetAllWays getAllWays =GetAllWays();
_getAllWays()async {
    SharedPreferences _prfes = await SharedPreferences.getInstance();

    setState(() {
      _isLoading = true;
    });
    FormData _headers = FormData.from({
      "lang": _prfes.getString('lang'),
    });
    Response response = await _util.get('getAllWays', headers: _headers);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        getAllWays = GetAllWays.fromJson(json.decode(response.toString()));
        _isLoading = false;
      });
    } else {}

}





//--------------------------------------------------------

  void _openCitiesBottomSheet(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: (MediaQuery.of(context).size.height / 2) + 100,
            child: CupertinoActionSheet(
              cancelButton: CupertinoButton(
                child: Text(
                  '${DemoLocalizations.of(context).trans('no')}',
                  style: TextStyle(
                      fontFamily: "Cairo",
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
  color:Theme.of(context).primaryColor                      
                      ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: getAllCities.data
                  .map(
                    (city) => CupertinoButton(
                          child: Center(
                            child: Text(
                              city.name ?? "",
                              style: TextStyle(
                                fontFamily: "Cairo",
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color:Theme.of(context).primaryColor
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            setState(() {
                              cit = city;
                            });
                          },
                        ),
                  )
                  .toList(),
            ),
          );
        });
  }

//------------------------intState------------------
  @override
  void initState() {
    connect();
    _getAllWays();
    _getAllCities();
    super.initState();
  }
//-------------------------inState------------------------

  //--------------------------------------------------
  void _getImage(BuildContext context, ImageSource source) {
    ImagePicker.pickImage(source: source, maxWidth: 400.0).then((File image) {
      setState(() {
        _imageFile = image;
      });
      Navigator.pop(context);
    });
  }

  void _openImagePicker(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 170,
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Text(
                '${DemoLocalizations.of(context).trans('choose')}',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                onPressed: () {
                  _getImage(context, ImageSource.camera);
                },
                child: Text(
                  '${DemoLocalizations.of(context).trans('camera')}',
                ),
              ),
              Divider(),
              FlatButton(
                onPressed: () {
                  _getImage(context, ImageSource.gallery);
                },
                child: Text(
                  '${DemoLocalizations.of(context).trans('gal')}',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onRememberMeChanged(bool newValue) => setState(() {
        rememberMe = newValue;

        if (rememberMe) {
        } else {}
      });
  _askUser() async {
    await showDialog(
        context: context,
        child: SimpleDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Center(
              child: Column(
            children: <Widget>[
              Text(
                "${DemoLocalizations.of(context).trans('client')}",
                style: TextStyle(
                    fontSize: 14, color: Theme.of(context).primaryColor),
              ),
            ],
          )),
          children: <Widget>[
            Column(
              children: <Widget>[
                SimpleDialogOption(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          border:
                              Border.all(color: Theme.of(context).primaryColor),
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "${DemoLocalizations.of(context).trans('check_trems')} ",
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('${DemoLocalizations.of(context).trans('ifo_shop')}'),
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: _isLoading
            ? Center(
                child: CupertinoActivityIndicator(
                  animating: true,
                  radius: 15,
                ),
              )
            : ListView(
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _image(),
                        shopName(),
                        textFiledPhone(),
                        Padding(
                          padding: const EdgeInsets.only(
                              right: 40, top: 10, left: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 13),
                                child: Text(
                                  '${DemoLocalizations.of(context).trans('from')}',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 13),
                                child: Text(
                                  '${DemoLocalizations.of(context).trans('to')}',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              )
                            ],
                          ),
                        ),
                        _time(),
                        _cities(),
                        _street(),
                        _location(),
                        _howKnowUs(),
                        nubmerDelegate(),
                        _terms(),
                        _register(),
                        SizedBox(
                          height: 30,
                        )
                      ],
                    ),
                  )
                ],
              ),
      ),
    );
  }

  Widget _image() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: _imageFile == null
          ? InkWell(
              onTap: () {
                _openImagePicker(context);
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 120.0),
                child: Container(
                  width: 120.0,
                  height: 120.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80.0),
                    // image: DecorationImage(
                    //   image: AssetImage('assets/photo.png'),
                    //   alignment: Alignment.center,

                    // ),
                    border: Border.all(color: Theme.of(context).primaryColor),
                  ),
                  child: Icon(
                    Icons.camera_alt,
                    size: 80,
                    color: Colors.grey,
                  ),
                ),
              ),
            )
          : InkWell(
              onTap: () {
                _openImagePicker(context);
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 120.0),
                child: Container(
                  width: 120.0,
                  height: 120.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: FileImage(_imageFile),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(80.0),
                      border: Border.all(color: Colors.white)),
                ),
              ),
            ),
    );
  }

  TimeOfDay startDate1 = TimeOfDay.now();
  Future<Null> _selectDate(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(disableAnimations: true),
          child: child,
        );
      },
      initialTime: TimeOfDay(hour: 10, minute: 47),
    );

    if (picked != null && picked != startDate1)
      setState(() {
        startDate1 = picked;
      });
  }

  TimeOfDay startDate2 = TimeOfDay.now();
  Future<Null> _selectDate2(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(disableAnimations: true),
          child: child,
        );
      },
      initialTime: TimeOfDay.now(),
    );

    if (picked != null && picked != startDate2)
      setState(() {
        startDate2 = picked;
      });
  }

  Widget _time() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        InkWell(
          onTap: () {
            _selectDate(context);
          },
          child: Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: Theme.of(context).primaryColor)),
              margin: EdgeInsets.only(right: 20, left: 20, top: 10),
              height: 50.0,
              child: Material(
                  elevation: 1.0,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 20, left: 10),
                        child: Image(
                          image: AssetImage('assets/date.png'),
                          height: 25,
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        '${startDate1.format(context).toString()}   ',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                    ],
                  ))),
        ),
        InkWell(
          onTap: () {
            _selectDate2(context);
          },
          child: Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: Theme.of(context).primaryColor)),
              margin: EdgeInsets.only(right: 20, left: 20, top: 10),
              height: 50.0,
              child: Material(
                  elevation: 1.0,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 20, left: 10),
                        child: Image(
                          image: AssetImage('assets/date.png'),
                          width: 25,
                          height: 25,
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text('${startDate2.format(context)}   ',
                          style:
                              TextStyle(color: Theme.of(context).primaryColor)),
                    ],
                  ))),
        )
      ],
    );
  }

  Widget _terms() {
    return Padding(
      padding: const EdgeInsets.only(right: 15, top: 20),
      child: Row(
        children: <Widget>[
          Checkbox(
              activeColor: Theme.of(context).primaryColor,
              value: rememberMe,
              onChanged: _onRememberMeChanged),
          Text(
            '${DemoLocalizations.of(context).trans('terms_accept')}',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontFamily: "Cairo",
            ),
          )
        ],
      ),
    );
  }

  Widget shopName() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        controller: _nameController,
        onSaved: (String value) {
          setState(() {
            name = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')} ';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        style: TextStyle(
            // fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 14),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              // fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/user.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('shop_name')}',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }

  Widget nubmerDelegate() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        controller: _delegateController,
        onSaved: (String value) {
          setState(() {
            delegate = value;
          });
        },
        // autovalidate: _autoValidate,
        // validator: (String value) {
        //   if (value.isEmpty || value.length < 4) {
        //     return '${DemoLocalizations.of(context).trans('vaild_phone')}';
        //   } else
        //     return null;
        // },
        keyboardType: TextInputType.phone,
        // controller: _prideNameController,
        style: TextStyle(
            // fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              // fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/money.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText:
                '  ${DemoLocalizations.of(context).trans('id_delegte')}   ',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 30),
          child: InkWell(
            onTap: () async {
              if (rememberMe == false) {
                _askUser();
              } else {
                var connectivityResult =
                    await Connectivity().checkConnectivity();
                if (connectivityResult == ConnectivityResult.mobile ||
                    connectivityResult == ConnectivityResult.wifi) {
                  _submit(context);
                } else {
                  showInSnackBar(
                      '${DemoLocalizations.of(context).trans('no_internet')}');
                }
              }
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  "${DemoLocalizations.of(context).trans('register')} ",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }

  Widget textFiledPhone() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        controller: _phoneController,
        onSaved: (String value) {
          setState(() {
            phone = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        // controller: _prideNameController,
        style: TextStyle(
            // fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              // fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/phone.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('phone')}',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget _cities() {
    return InkWell(
      onTap: () {
        _openCitiesBottomSheet(context);
      },
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
            elevation: 1.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 20, left: 9),
                  child: Image(
                    image: AssetImage('assets/location.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Text(
                    cit == null
                        ? "${DemoLocalizations.of(context).trans('Citi')}"
                        : cit.name ?? "",
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                ),
                Icon(Icons.arrow_drop_down, color: Colors.grey),
              ],
            ),
          )),
    );
  }

  Widget _street() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        controller: _stateController,
        onSaved: (String value) {
          setState(() {
            state = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty ) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')}';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 25.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/location.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '  ${DemoLocalizations.of(context).trans('street')}   ',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget _location() {
    return InkWell(
      onTap: () {
        Navigator.push(
                context, MaterialPageRoute(builder: (context) => MapScreen()))
            .then((location) {
          print("----------------------$location");
          print("LAAAAATTTTTT" + '${location['lat']}' + 'asdasdasdasdasd');
          print("LLLLNNNNN" + '${location['long']}' + 'dsdsdfsdfsdfsdfsdfsdf');
          setState(() {
            lat = location['lat'];
            print(lat.toString() + 'jjjjjjjjjjjjjjjjjjjjjjjjjjj');
            lang = location['long'];
            address = location['address'];
          });
        });
      },
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
            elevation: 1.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 20, left: 10),
                  child: Image(
                    image: AssetImage('assets/location.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: address != null
                      ? Text(
                          address,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        )
                      : Text(
                          '${DemoLocalizations.of(context).trans('loca')}',
                          style: TextStyle(color: Colors.grey),
                        ),
                ),
                Icon(Icons.arrow_drop_down, color: Colors.grey),
              ],
            ),
          )),
    );
  }
void _openWaysBottomSheet(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: (MediaQuery.of(context).size.height / 2) + 100,
            child: CupertinoActionSheet(
              cancelButton: CupertinoButton(
                child: Text(
                  '${DemoLocalizations.of(context).trans('no')}',
                  style: TextStyle(
                      fontFamily: "Cairo",
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
  color:Theme.of(context).primaryColor                      
                      ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: getAllWays.getAllWaysData
                  .map(
                    (data) => CupertinoButton(
                          child: Center(
                            child: Text(
                              data.name ?? "",
                              style: TextStyle(
                                fontFamily: "Cairo",
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color:Theme.of(context).primaryColor
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            setState(() {
                              getAllWaysData = data;
                            });
                          },
                        ),
                  )
                  .toList(),
            ),
          );
        });
  } 
  Widget _howKnowUs() {
    return InkWell(
      onTap: () {
        _openWaysBottomSheet(context);
      },
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
            elevation: 1.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 20, left: 10),
                  child: Image(
                    image: AssetImage('assets/questionmark.png'),
                    height: 20,
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child:
                    
                    Text( 
                     getAllWaysData == null ?
                      '${DemoLocalizations.of(context).trans('knowapp')}':
                       getAllWaysData.name??'',
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                    
                   
                    ),
                Icon(Icons.arrow_drop_down, color: Colors.grey),
              ],
            ),
          )),
    );
  }
}
