import 'dart:convert';
import 'package:delivery_guide/Iformation_Shop.dart';
import 'package:delivery_guide/Models/getAllCategories.dart';
import 'package:delivery_guide/Raking.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterShop extends StatefulWidget {
  @override
  _RegisterShopState createState() => _RegisterShopState();
}

class _RegisterShopState extends State<RegisterShop> {
 /////------------------------------------------------------------
  bool _isLoading = false;
  NetworkUtil _util = NetworkUtil();
  GetAllCategories getAllCategories = GetAllCategories();
  _getAllCategories() async {
    SharedPreferences _prfes = await SharedPreferences.getInstance();
    setState(() {
      _isLoading = true;
    });
    FormData _headers = FormData.from({
      "lang": _prfes.getString('lang'),
    });
    Response response = await _util.get('getAllCategories', headers: _headers);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        getAllCategories =
            GetAllCategories.fromJson(json.decode(response.toString()));
        _isLoading = false;
      });
    } else {}
  }

  @override
  void initState() {
    _getAllCategories();
    super.initState();
  }

  /////--------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
            ? TextDirection.ltr
            : TextDirection.rtl,
        child: Scaffold(
            appBar: AppBar(
              title: Text("${DemoLocalizations.of(context).trans('rshop')}"),
              centerTitle: true,
              leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios),
              ),
            ),
            body: _isLoading
                ? Center(
                    child: CupertinoActivityIndicator(
                      animating: true,
                      radius: 15,
                    ),
                  )
                : Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: <Widget>[
                        _image(),

                        getAllCategories.data.length > 0
                            ? Expanded(
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: getAllCategories.data.length,
                                  itemBuilder: (context, index) {
                                    return card(
                                        getAllCategories.data[index].name,
                                        getAllCategories.data[index].id);
                                  },
                                ),
                              )
                            : Center(
                                child: Text(
                                    "${DemoLocalizations.of(context).trans('no_data')}"),
                              ),
                        cardElse(),
                        _image2()
                      ],
                    ),
                  )));
  }

  Widget card(String name, int id) {
    return InkWell(
      onTap: () {
        
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => InformationShop(
                      cateid: id,
                    )));
      },
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
              elevation: 1.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0))),
              child: Center(
                child: Text(
                  name,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                ),
              ))),
    );
  }

  Widget cardElse() {
    return InkWell(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => Raking())),
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
              elevation: 1.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0))),
              child: Center(
                child: Text(
                  'تصنيف اخر',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                ),
              ))),
    );
  }

  Widget _image() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Container(
        height: 55,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/logo.png'))),
      ),
    );
  }

  Widget _image2() {
    return Padding(
      padding: EdgeInsets.only(top: 50),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Image(
          image: AssetImage('assets/bike.png'),
          width: 150,
          height: 100,
        ),
      ),
    );
  }
}
