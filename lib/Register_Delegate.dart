import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/HomePageScreen.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'Models/Market/setNewDelegate.dart';

class RegisterDelegate extends StatefulWidget {
  @override
  _RegisterDelegateState createState() => _RegisterDelegateState();
}

class _RegisterDelegateState extends State<RegisterDelegate> {
  bool selected;
  bool _autoValidate = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String name, phone, identity, bank_name, bank_account_no;

  var connectivityResult;

  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  NetworkUtil _util = NetworkUtil();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _identityController = TextEditingController();
  TextEditingController _bankAccountNoController = TextEditingController();
  TextEditingController _banknameController = TextEditingController();
  showScussesDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "${DemoLocalizations.of(context).trans('scuess')}",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: Column(
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontFamily: "Cairo",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    connect();
    super.initState();
  }

  void showLoadingDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text(
                      "${DemoLocalizations.of(context).trans('wait')}",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  SetNewDelegate setNewDelegate;
  _submit(BuildContext context) async {
    // _formKey.currentState.save();
    FormState formState = _formKey.currentState;

    if (formState.validate()) {
      print(name);
      print(phone);
      print(identity);
      print(bank_account_no);
      print(bank_name);
      formState.save();

      FormData _formData = FormData.from({
        "name": name,
        "phone": phone,
        "identity": identity,
        "bank_name": bank_name,
        "bank_account_no": bank_account_no,
      });

      FormData _headers = FormData.from({
        "Content-Type": "application/json",
      });
      showLoadingDialog();

      Response response = await _util.post('setNewDelegate',
          body: _formData, headers: _headers);
      Navigator.pop(context);

      if (response.statusCode == 200) {
        setNewDelegate =
            SetNewDelegate.fromJson(json.decode(response.toString()));
        Navigator.pop(context);

        showScussesDialog();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePageScreen()));
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              print(response.statusMessage);
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                title: Text(response.statusMessage),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            '${DemoLocalizations.of(context).trans('rdelegate')}',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  textFiledName(),
                  textFiledPhone(),
                  nubmerDelegate(),
                  bankName(),
                  bankNamber(),
                  _terms(),
                  _delegatebutton()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget textFiledName() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            name = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')}';
          } else
            return null;
        },
        controller: _nameController,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 5.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/user.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText:
                '${DemoLocalizations.of(context).trans('name_delegate')}',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget textFiledPhone() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            phone = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        controller: _phoneController,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/phone.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('phone')}',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget nubmerDelegate() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            identity = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        controller: _identityController,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/id.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('id_delegte')}',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget bankName() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            bank_name = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 1) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')}';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        controller: _banknameController,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/bank.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('bank')}   ',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget bankNamber() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            bank_account_no = value;
          });
        },
        autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        controller: _bankAccountNoController,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/money.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: "${DemoLocalizations.of(context).trans('bank_num')}",
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  bool rememberMe = false;
  void _onRememberMeChanged(bool newValue) => setState(() {
        rememberMe = newValue;

        if (rememberMe) {
        } else {}
      });
  _askUser() async {
    await showDialog(
        context: context,
        child: SimpleDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Center(
              child: Column(
            children: <Widget>[
              Text(
                " ${DemoLocalizations.of(context).trans('client')}",
                style: TextStyle(
                    fontSize: 14, color: Theme.of(context).primaryColor),
              ),
            ],
          )),
          children: <Widget>[
            Column(
              children: <Widget>[
                SimpleDialogOption(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          border:
                              Border.all(color: Theme.of(context).primaryColor),
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        " ${DemoLocalizations.of(context).trans('check_trems')}",
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ));
  }

  Widget _terms() {
    return Padding(
      padding: const EdgeInsets.only(right: 15, top: 20),
      child: Row(
        children: <Widget>[
          Checkbox(
              activeColor: Theme.of(context).primaryColor,
              value: rememberMe,
              onChanged: _onRememberMeChanged),
          Text(
            '${DemoLocalizations.of(context).trans('terms_accept')}',
            style: TextStyle(color: Theme.of(context).primaryColor),
          )
        ],
      ),
    );
  }

  Widget _delegatebutton() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 30),
          child: InkWell(
            onTap: () async {
              if (rememberMe == false) {
                _askUser();
              } else {
                var connectivityResult =
                    await Connectivity().checkConnectivity();
                if (connectivityResult == ConnectivityResult.mobile ||
                    connectivityResult == ConnectivityResult.wifi) {
                  _submit(context);
                } else {
                  showInSnackBar(
                      '${DemoLocalizations.of(context).trans('no_internet')}');
                }
              }
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  '${DemoLocalizations.of(context).trans('register')}',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }
}
