import 'package:delivery_guide/EndDrawer/End_Drawer.dart';
import 'package:delivery_guide/Mada.dart';
import 'package:delivery_guide/Serial_number.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'localization/DemoLocalization.dart';

class Subcribtions extends StatefulWidget {
  @override
  _SubcribtionsState createState() => _SubcribtionsState();
}

class _SubcribtionsState extends State<Subcribtions> {
  int _groupValue = -1;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
          drawer: SizedBox(
               width: MediaQuery.of(context).size.width *.55,
            height: MediaQuery.of(context).size.height,
                      child: Drawer(
              child: EndDrawer().endDrawer(
                  Theme.of(context).primaryColor, context, '/register'),
            ),
          ),
          appBar: AppBar(
            title: Text(' ${DemoLocalizations.of(context).trans('pament')}',
                style: TextStyle(
                  color: Colors.white,
                )),
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
          ),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 10, top: 20, left: 10),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border:
                          Border.all(color: Theme.of(context).primaryColor)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _myRadioButton(
                        value: 0,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                      ),
                      Text(
                        '${DemoLocalizations.of(context).trans('mada')}',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 40, left: 10),
                        child: Image(
                          image: AssetImage('assets/mada.png'),
                          height: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10, top: 20, left: 10),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border:
                          Border.all(color: Theme.of(context).primaryColor)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _myRadioButton(
                        value: 1,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                      ),
                      Text(
                        '${DemoLocalizations.of(context).trans('serial')}',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 40, left: 10),
                        child: Image(
                          image: AssetImage('assets/payment.png'),
                          height: 35,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              _confirm()
            ],
          )),
    );
  }

  Widget _myRadioButton({int value, Function onChanged}) {
    return Radio(
      value: value,
      groupValue: _groupValue,
      onChanged: onChanged,
    );
  }

  Widget _confirm() {
    return InkWell(
      onTap: () {
        if (_groupValue == -1) {
          _askUser();
        } else if (_groupValue == 0) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Mada()));
        } else {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SerialNumber()));
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 350, right: 10, left: 10),
        child: Container(
          height: 50,
          width: 150,
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(30),
            gradient: LinearGradient(
              // Where the linear gradient begins and ends
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,

              // Add one stop for each color. Stops should increase from 0 to 1
              stops: [0.5, 0.9],
              colors: [
                // Colors are easy thanks to Flutter's Colors class.
                Color(getColorHexFromStr('#E2548E')),
                Color(
                  getColorHexFromStr("#F5AFBA"),
                ),
              ],
            ),
          ),
          child: Center(
              child: Text(
            ' ${DemoLocalizations.of(context).trans('pament')}',
            style: TextStyle(color: Colors.white),
          )),
        ),
      ),
    );
  }

  _askUser() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            title: Center(
                child: Column(
              children: <Widget>[
                Text(
                  " عميلنا العزيز",
                  style: TextStyle(
                      fontSize: 14, color: Theme.of(context).primaryColor),
                ),
                Text(
                  "",
                  style: TextStyle(
                      fontSize: 14, color: Theme.of(context).primaryColor),
                ),
              ],
            )),
            children: <Widget>[
              Column(
                children: <Widget>[
                  SimpleDialogOption(
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.circular(30)),
                        child: Center(
                            child: Text(
                          "برجاء التأكد من اختيار طريقه الدفع ",
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ))),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ],
          );
        });
  }
}
