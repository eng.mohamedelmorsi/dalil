import 'package:delivery_guide/HomePageScreen.dart';
import 'package:delivery_guide/Language.dart';
import 'package:delivery_guide/Subscribtion.dart';
import 'package:delivery_guide/Trems_Screen.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';








class EndDrawer {
  endDrawer(Color color, BuildContext context, String router2) {
    return Drawer(
        child: ListView(
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 25),
        child: Container(
          height: 70,
          width: 122,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/logo.png'))),
        ),
      ),
      InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => HomePageScreen()));
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 30, top: 20, left: 30),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/home.png'),
                width: 30,
                height: 30,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans('home_page')}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      InkWell(
        onTap: () {
          Navigator.pushNamed(context, router2);
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 30, top: 20, left: 30),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/login.png'),
                width: 30,
                height: 30,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans('register')}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      InkWell(
        onTap: () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => Language())),
        child: Padding(
          padding: const EdgeInsets.only(right: 30, top: 20, left: 30),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/language.png'),
                width: 25,
                height: 22,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans('lang')}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Subcribtions()));
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 30, top: 20, left: 30),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/money.png'),
                width: 25,
                height: 25,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans('pament')}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      InkWell(
        onTap: () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => Terms())),
        child: Padding(
          padding: const EdgeInsets.only(right: 30, top: 20, left: 30),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/terms.png'),
                width: 25,
                height: 25,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans("Terms")}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      InkWell(
        onTap: () {
          Share.share('check out my website https://example.com');
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 29, top: 20, left: 29),
          child: Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/share.png'),
                width: 25,
                height: 25,
                color: color,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${DemoLocalizations.of(context).trans('share')}",
                style: TextStyle(color: color),
              )
            ],
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 120),
        child: Align(
          child: Image(
            alignment: Alignment.center,
            image: AssetImage('assets/bike.png'),
            width: 200,
            height: 120,
          ),
        ),
      )
    ],
        ),
      );
  }
}
