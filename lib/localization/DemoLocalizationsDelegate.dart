import 'dart:async';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';



class DemoLocalizationsDelegate
    extends LocalizationsDelegate<DemoLocalizations> {
  const DemoLocalizationsDelegate();
  @override
  bool isSupported(Locale locale) =>
      ['ar', 'en'].contains(locale.languageCode);

  @override
  Future<DemoLocalizations> load(Locale locale) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _lng = (prefs.getString('lang') ?? "ar");
    DemoLocalizations localizations = new DemoLocalizations(Locale(_lng));
    await localizations.load();

    print("Load ${locale.languageCode}");
    return localizations;
  }
  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}
