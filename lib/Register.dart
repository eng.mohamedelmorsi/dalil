import 'package:delivery_guide/EndDrawer/End_Drawer.dart';
import 'package:delivery_guide/Register_Delegate.dart';
import 'package:delivery_guide/Register_Shop.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        drawer: SizedBox(
             width: MediaQuery.of(context).size.width *.55,
            height: MediaQuery.of(context).size.height,
                  child: Drawer(
            child: EndDrawer()
                .endDrawer(Theme.of(context).primaryColor, context, '/register'),
          ),
        ),
        appBar: AppBar(
          title: Text('${DemoLocalizations.of(context).trans('register')}'),
          centerTitle: true,
        ),
        body: ListView(
          children: <Widget>[
            _image(),
            _shopbutton(),
            _delegatebutton(),
            _image2()
          ],
        ),
      ),
    );
  }

  Widget _image() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/logo.png'))),
      ),
    );
  }

  Widget _shopbutton() {
    return Center(
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 70),
          child: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RegisterShop()));
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  "${DemoLocalizations.of(context).trans('rshop')}",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }

  Widget _delegatebutton() {
    return Center(
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 30),
          child: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RegisterDelegate()));
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  "${DemoLocalizations.of(context).trans('rdelegate')}",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }

  Widget _image2() {
    return Padding(
      padding: const EdgeInsets.only(top: 50),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Image(
          image: AssetImage('assets/bike.png'),
          width: 200,
        ),
      ),
    );
  }
}
