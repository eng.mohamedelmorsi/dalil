import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/widgets/shop_rating_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'localization/DemoLocalization.dart';

class ShopName extends StatefulWidget {
  final String name, phone,
   start, end, image;
  final int id,distance;
  final double rate;
  final double lat;
  final double long;

  const ShopName(
      {Key key,
      this.lat,
      this.long,
      this.name,
      this.phone,
      this.start,
      this.end,
      this.id,
      this.distance,
      this.image,
      this.rate})
      : super(key: key);
  @override
  _ShopNameState createState() => _ShopNameState();
}

class _ShopNameState extends State<ShopName> {
  BitmapDescriptor myIcon;
  GoogleMapController myController;
  double currentLat;
  double currentLong;
  dynamic currentAddress;
  Marker marker = Marker(
    markerId: MarkerId("1"),
  );
  Set<Marker> mark = Set();
  var connectivityResult;
  var location = Location();
  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }
  void initState() {
    super.initState();
    LocationPermissions().requestPermissions().then((PermissionStatus status) {
      if (status == PermissionStatus.granted) {
        location.getLocation().then((LocationData myLocation) {
          setState(() {
            currentLat = widget.lat;
            currentLong = widget.long;

            InfoWindow infoWindow = InfoWindow(title: "Location");
            Marker marker = Marker(
              draggable: true,
              markerId: MarkerId('markers.length.toString()'),
              infoWindow: infoWindow,
              position: LatLng( widget.lat , widget.long),
              icon: myIcon,
            );
            setState(() {
              mark.add(marker);
            });
          });
        });
      } else {}
    });
    BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
        size: Size(
          0,
          0,
        ),
        devicePixelRatio: .9,
      ),
      'assets/nnnn.png',
    ).then((onValue) {
      myIcon = onValue;
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      myController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.name,
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15),
              child: InkWell(
                onTap: () {
                  Share.share('check out my website https://example.com');
                },
                child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Image(
                      image: AssetImage('assets/share.png'),
                      height: 25,
                      width: 25,
                    )),
              ),
            )
          ],
        ),
        body: currentLat == null && currentLong == null
            ? Center(
                child: CupertinoActivityIndicator(
                animating: true,
                radius: 15,
              ))
            : ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 200,
                          width: MediaQuery.of(context).size.width,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/logo.png',
                            image: widget.image,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          border:
                              Border.all(color: Colors.grey.withOpacity(.2)),
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 20, left: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10, right: 10, left: 10),
                                  child: Text(
                                    widget.name,
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                SizedBox(
                                  width: 56,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: InkWell(
                                    onTap: () {
                                      UrlLauncher.launch(
                                          'tel:+${widget.phone}');
                                    },
                                    child: Container(
                                      height: 30,
                                      width: 90,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Image(
                                            image:
                                                AssetImage('assets/call.png'),
                                            width: 20,
                                            height: 20,
                                          ),
                                          Text(
                                            '${DemoLocalizations.of(context).trans('contact')}',
                                            style:
                                                TextStyle(color: Colors.white),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(
                                        top: 20, left: 5, right: 5, bottom: 2),
                                    child: Container(
                                        height: 30,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        child: InkWell(
                                            onTap: () {
                                              showDialog(
                                                  context: context,

                                                  //  barrierDismissible: false,
                                                  barrierDismissible:
                                                      true, // set to false if you want to force a rating
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      content: ShopRatingWidget(
                                                        id: widget.id,
                                                      ),
                                                    );
                                                  });
                                            },
                                            child: Icon(
                                              Icons.star,
                                              color: Colors.white,
                                            )))),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 10, left: 10),
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                        'assets/time.png',
                                      ),
                                      height: 19,
                                    ),
                                    SizedBox(
                                      width: 12,
                                    ),
                                    Text(
                                      " ${DemoLocalizations.of(context).trans('fro')}  " +
                                          widget.start +
                                          "   "
                                              " ${DemoLocalizations.of(context).trans('t')} " +
                                          widget.end,
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 10, top: 10, left: 10),
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                        'assets/location.png',
                                      ),
                                      height: 20,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      '${DemoLocalizations.of(context).trans('distance')}  "${widget.distance}" ${DemoLocalizations.of(context).trans('km')}',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          _cardLocations(),
                          SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }

  Widget _cardLocations() {
    return currentLat == null && currentLong == null
        ? Center(
            child: CupertinoActivityIndicator(
            animating: true,
            radius: 15,
          ))
        : Padding(
            padding: const EdgeInsets.only(top: 20, right: 15, left: 15),
            child: Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              child: GoogleMap(
                markers: mark,
                initialCameraPosition: CameraPosition(
                    target: LatLng(currentLat, currentLong), zoom: 15.0),
                onMapCreated: _onMapCreated,
                onCameraMove: ((_position) => _updatePosition(_position)),
              ),
            ),
          );
  }

  void _updatePosition(CameraPosition _position) {
    Position newMarkerPosition = Position(
        latitude: _position.target.latitude,
        longitude: _position.target.longitude);
    setState(() {
      marker = marker.copyWith(
          positionParam:
              LatLng(newMarkerPosition.latitude, newMarkerPosition.longitude));
    });
  }

 
}
