// To parse this JSON data, do
//
//     final getAllCities = getAllCitiesFromJson(jsonString);

import 'dart:convert';

GetAllCities getAllCitiesFromJson(String str) => GetAllCities.fromJson(json.decode(str));

String getAllCitiesToJson(GetAllCities data) => json.encode(data.toJson());

class GetAllCities {
    List<Datum> data;
    String status;
    String message;

    GetAllCities({
        this.data,
        this.status,
        this.message,
    });

    factory GetAllCities.fromJson(Map<String, dynamic> json) => new GetAllCities(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
        "message": message,
    };
}

class Datum {
    int id;
    String name;

    Datum({
        this.id,
        this.name,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
