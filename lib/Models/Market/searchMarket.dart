// To parse this JSON data, do
//
//     final searchMarket = searchMarketFromJson(jsonString);

import 'dart:convert';

SearchMarket searchMarketFromJson(String str) => SearchMarket.fromJson(json.decode(str));

String searchMarketToJson(SearchMarket data) => json.encode(data.toJson());

class SearchMarket {
   List<Datum> data;
    String message;
    String status;

    SearchMarket({
        this.data,
        this.message,
        this.status,
    });

    factory SearchMarket.fromJson(Map<String, dynamic> json) => new SearchMarket(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message,
        "status": status,
    };
}

class Datum {
    int id;
    String name;
    String phone;
    String latitude;
    String longitude;
    double rate;
    String startTime;
    String endTime;
    String image;
    int distance;

    Datum({
        this.id,
        this.name,
        this.phone,
        this.latitude,
        this.longitude,
        this.rate,
        this.startTime,
        this.endTime,
        this.image,
        this.distance
    });

    factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        id: json["id"],
        name: json["name"],
        phone: json["phone"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        rate: json["rate"].toDouble(),
        startTime: json["start_time"],
        endTime: json["end_time"],
        image: json["image"],
        distance: json['distance']
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone": phone,
        "latitude": latitude,
        "longitude": longitude,
        "rate": rate,
        "start_time": startTime,
        "end_time": endTime,
        "image": image,
    };
}
