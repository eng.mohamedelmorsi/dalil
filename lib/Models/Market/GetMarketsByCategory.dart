// To parse this JSON data, do
//
//     final getMarketsByCategory = getMarketsByCategoryFromJson(jsonString);

import 'dart:convert';

GetMarketsByCategory getMarketsByCategoryFromJson(String str) => GetMarketsByCategory.fromJson(json.decode(str));

String getMarketsByCategoryToJson(GetMarketsByCategory data) => json.encode(data.toJson());

class GetMarketsByCategory {
    List<Datum> data;
    String status;
    String message;

    GetMarketsByCategory({
        this.data,
        this.status,
        this.message,
    });

    factory GetMarketsByCategory.fromJson(Map<String, dynamic> json) => new GetMarketsByCategory(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
        "message": message,
    };
}

class Datum {
    int id;
    String name;
    String phone;
    String latitude;
    String longitude;
    double rate;
    String startTime;
    String endTime;
    String image;
    int distance;
    

    Datum({
        this.id,
        this.name,
        this.phone,
        this.latitude,
        this.longitude,
        this.rate,
        this.startTime,
        this.endTime,
        this.image,
        this.distance
    });

    factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        id: json["id"],
        name: json["name"],
        phone: json["phone"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        rate: json["rate"].toDouble(),
        startTime: json["start_time"],
        endTime: json["end_time"],
        image: json["image"],
        distance: json['distance']
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone": phone,
        "latitude": latitude,
        "longitude": longitude,
        "rate": rate,
        "start_time": startTime,
        "end_time": endTime,
        "image": image,
    };
}
