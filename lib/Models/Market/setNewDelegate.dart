// To parse this JSON data, do
//
//     final setNewDelegate = setNewDelegateFromJson(jsonString);

import 'dart:convert';

SetNewDelegate setNewDelegateFromJson(String str) => SetNewDelegate.fromJson(json.decode(str));

String setNewDelegateToJson(SetNewDelegate data) => json.encode(data.toJson());

class SetNewDelegate {
    Data data;
    String message;
    String status;

    SetNewDelegate({
        this.data,
        this.message,
        this.status,
    });

    factory SetNewDelegate.fromJson(Map<String, dynamic> json) => new SetNewDelegate(
        data: Data.fromJson(json["data"]),
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "message": message,
        "status": status,
    };
}

class Data {
    String name;
    String phone;
    String identity;
    String bankName;
    String bankAccountNo;
    bool isAgree;

    Data({
        this.name,
        this.phone,
        this.identity,
        this.bankName,
        this.bankAccountNo,
        this.isAgree,
    });

    factory Data.fromJson(Map<String, dynamic> json) => new Data(
        name: json["name"],
        phone: json["phone"],
        identity: json["identity"],
        bankName: json["bank_name"],
        bankAccountNo: json["bank_account_no"],
        isAgree: json["is_agree"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "identity": identity,
        "bank_name": bankName,
        "bank_account_no": bankAccountNo,
        "is_agree": isAgree,
    };
}
