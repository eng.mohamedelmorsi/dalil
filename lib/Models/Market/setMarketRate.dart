// To parse this JSON data, do
//
//     final setMarketRate = setMarketRateFromJson(jsonString);

import 'dart:convert';

SetMarketRate setMarketRateFromJson(String str) => SetMarketRate.fromJson(json.decode(str));

String setMarketRateToJson(SetMarketRate data) => json.encode(data.toJson());

class SetMarketRate {
    List<dynamic> data;
    String message;
    String status;

    SetMarketRate({
        this.data,
        this.message,
        this.status,
    });

    factory SetMarketRate.fromJson(Map<String, dynamic> json) => new SetMarketRate(
        data: new List<dynamic>.from(json["data"].map((x) => x)),
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x)),
        "message": message,
        "status": status,
    };
}
