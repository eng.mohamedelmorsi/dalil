// To parse this JSON data, do
//
//     final getAllWays = getAllWaysFromJson(jsonString);

import 'dart:convert';

GetAllWays getAllWaysFromJson(String str) => GetAllWays.fromJson(json.decode(str));

String getAllWaysToJson(GetAllWays data) => json.encode(data.toJson());

class GetAllWays {
    List<GetAllWaysData> getAllWaysData;
    String status;
    String message;

    GetAllWays({
        this.getAllWaysData,
        this.status,
        this.message,
    });

    factory GetAllWays.fromJson(Map<String, dynamic> json) => new GetAllWays(
        getAllWaysData: new List<GetAllWaysData>.from(json["data"].map((x) => GetAllWaysData.fromJson(x))),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(getAllWaysData.map((x) => x.toJson())),
        "status": status,
        "message": message,
    };
}

class GetAllWaysData {
    int id;
    String name;

    GetAllWaysData({
        this.id,
        this.name,
    });

    factory GetAllWaysData.fromJson(Map<String, dynamic> json) => new GetAllWaysData(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
