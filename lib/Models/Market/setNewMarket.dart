// To parse this JSON data, do
//
//     final setNewMarket = setNewMarketFromJson(jsonString);

import 'dart:convert';

SetNewMarket setNewMarketFromJson(String str) => SetNewMarket.fromJson(json.decode(str));

String setNewMarketToJson(SetNewMarket data) => json.encode(data.toJson());

class SetNewMarket {
    Data data;
    String message;
    String status;

    SetNewMarket({
        this.data,
        this.message,
        this.status,
    });

    factory SetNewMarket.fromJson(Map<String, dynamic> json) => new SetNewMarket(
        data: Data.fromJson(json["data"]),
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "message": message,
        "status": status,
    };
}

class Data {
    String name;
    String phone;
    String categoryId;
    String startTime;
    String endTime;
    String image;

    Data({
        this.name,
        this.phone,
        this.categoryId,
        this.startTime,
        this.endTime,
        this.image,
    });

    factory Data.fromJson(Map<String, dynamic> json) => new Data(
        name: json["name"],
        phone: json["phone"],
        categoryId: json["category_id"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "category_id": categoryId,
        "start_time": startTime,
        "end_time": endTime,
        "image": image,
    };
}
