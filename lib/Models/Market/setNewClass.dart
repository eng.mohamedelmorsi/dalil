// To parse this JSON data, do
//
//     final setNewClassification = setNewClassificationFromJson(jsonString);

import 'dart:convert';

SetNewClassification setNewClassificationFromJson(String str) => SetNewClassification.fromJson(json.decode(str));

String setNewClassificationToJson(SetNewClassification data) => json.encode(data.toJson());

class SetNewClassification {
    Data data;
    String message;
    String status;

    SetNewClassification({
        this.data,
        this.message,
        this.status,
    });

    factory SetNewClassification.fromJson(Map<String, dynamic> json) => new SetNewClassification(
        data: Data.fromJson(json["data"]),
        message: json["message"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "message": message,
        "status": status,
    };
}

class Data {
    String shopField;
    String shopName;
    String phone;
    String cityId;
    String state;

    Data({
        this.shopField,
        this.shopName,
        this.phone,
        this.cityId,
        this.state,
    });

    factory Data.fromJson(Map<String, dynamic> json) => new Data(
        shopField: json["shop_field"],
        shopName: json["shop_name"],
        phone: json["phone"],
        cityId: json["city_id"],
        state: json["state"],
    );

    Map<String, dynamic> toJson() => {
        "shop_field": shopField,
        "shop_name": shopName,
        "phone": phone,
        "city_id": cityId,
        "state": state,
    };
}
