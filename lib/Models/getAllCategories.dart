// To parse this JSON data, do
//
//     final getAllCategories = getAllCategoriesFromJson(jsonString);

import 'dart:convert';

GetAllCategories getAllCategoriesFromJson(String str) => GetAllCategories.fromJson(json.decode(str));

String getAllCategoriesToJson(GetAllCategories data) => json.encode(data.toJson());

class GetAllCategories {
    List<Datum> data;
    String status;
    String message;

    GetAllCategories({
        this.data,
        this.status,
        this.message,
    });

    factory GetAllCategories.fromJson(Map<String, dynamic> json) => new GetAllCategories(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
        "message": message,
    };
}

class Datum {
    int id;
    String name;
    String image;

    Datum({
        this.id,
        this.name,
        this.image,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        id: json["id"],
        name: json["name"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
    };
}
