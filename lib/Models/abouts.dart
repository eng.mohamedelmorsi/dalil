// To parse this JSON data, do
//
//     final getAllTerms = getAllTermsFromJson(jsonString);

import 'dart:convert';

GetAllTerms getAllTermsFromJson(String str) => GetAllTerms.fromJson(json.decode(str));

String getAllTermsToJson(GetAllTerms data) => json.encode(data.toJson());

class GetAllTerms {
    List<Datum> data;
    String status;
    String message;

    GetAllTerms({
        this.data,
        this.status,
        this.message,
    });

    factory GetAllTerms.fromJson(Map<String, dynamic> json) => new GetAllTerms(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
        "message": message,
    };
}

class Datum {
    int id;
    String name;
    String description;

    Datum({
        this.id,
        this.name,
        this.description,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        id: json["id"],
        name: json["name"],
        description: json["description"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
    };
}
