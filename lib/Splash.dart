import 'dart:io';
import 'package:delivery_guide/HomePageScreen.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:connectivity/connectivity.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Timer(Duration(seconds: 2), () async {
      var connectivityResult = await Connectivity().checkConnectivity();
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePageScreen()));
        return true;
      } else {
        showInSnackBar('${DemoLocalizations.of(context).trans('no_internet')}');
        return false;
      }
    });
    super.initState();
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: InkWell(
            onTap: () {
              exit(0);
            },
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: "Cairo",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/splash.png'), fit: BoxFit.cover),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage('assets/logo.png'),
              height: 100,
            ),
          ),
        ),
      ),
    );
  }
}
