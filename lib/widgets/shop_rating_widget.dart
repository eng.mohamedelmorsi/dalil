import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/Models/Market/setMarketRate.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:delivery_guide/widgets/rating_widget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

class ShopRatingWidget extends StatefulWidget {
  final int id;
  ShopRatingWidget({Key key, this.id}) : super(key: key);

  _ShopRatingWidgetState createState() => _ShopRatingWidgetState();
}

class _ShopRatingWidgetState extends State<ShopRatingWidget> {
  double shopRate = 0.0;
  bool value = true;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String comment;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController _rateController = TextEditingController();
  TextEditingController _commentController = TextEditingController();
  NetworkUtil _util = NetworkUtil();
  void _onRatingChange(double rate) {
    print("rate $rate");
    setState(() {
      shopRate = rate;
    });
  }

  var connectivityResult;
  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  void showLoadingDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text(
                      "${DemoLocalizations.of(context).trans('wait')}",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: InkWell(
            onTap: () {},
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: "Cairo",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showScussesDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                     "${DemoLocalizations.of(context).trans('scuess')}" ,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  SetMarketRate setNewMarket;
  _submit() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    FormData _formData = FormData.from(
        {"rate": shopRate, "comment": comment, "market_id": widget.id});
    FormData _headers = FormData.from({'Content-Type': 'application/json'});

    showLoadingDialog();
    _util
        .post('setMarketRate', body: _formData, headers: _headers)
        .then((result) {
      Navigator.pop(context);

      if (result.statusCode == 200) {
        setNewMarket = SetMarketRate.fromJson(json.decode(result.toString()));
        Navigator.pop(context);
        showScussesDialog();
      } else {
        print(result.data['message'] + "asasdasdasdasdasdasd");
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 50,
                title: Text(
                  result.data['message'],
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 15),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    }).catchError((err) {
      print("333333333333333333333333#########" + err.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        width: MediaQuery.of(context).size.width,
        height: 200,
        child: _SystemPadding(
            child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "${DemoLocalizations.of(context).trans('msg')}",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 20),
                ),
                SmoothStarRating(
                  starCount: 5,
                  rating: shopRate,
                  onRatingChanged: _onRatingChange,
                  color: Theme.of(context).primaryColor,
                  borderColor: Theme.of(context).primaryColor,
                  size: 25,
                  allowHalfRating: true,
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: TextFormField(
                    controller: _rateController,
                    validator: (val) {
                      if (val.isEmpty) {
                        return '${DemoLocalizations.of(context).trans('vaild')}';
                      } else
                        return null;
                    },
                    maxLines: 2,
                    onSaved: (val) {
                      comment = val;
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7),
                          borderSide: BorderSide(
                              style: BorderStyle.solid,
                              color: Theme.of(context).primaryColor)),
                      hintText:
                          ' ${DemoLocalizations.of(context).trans('vaild')} ',
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 30,
                  margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      // Where the linear gradient begins and ends
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,

                      // Add one stop for each color. Stops should increase from 0 to 1
                      stops: [0.5, 0.9],
                      colors: [
                        // Colors are easy thanks to Flutter's Colors class.
                        Color(getColorHexFromStr('#E2548E')),
                        Color(
                          getColorHexFromStr("#F5AFBA"),
                        ),
                      ],
                    ),
                  ),
                  child: InkWell(
                      onTap: () async {
                        var connectivityResult =
                            await Connectivity().checkConnectivity();
                        if (connectivityResult == ConnectivityResult.mobile ||
                            connectivityResult == ConnectivityResult.wifi) {
                          _submit();
                        } else {
                          showInSnackBar(
                              '${DemoLocalizations.of(context).trans('no_internet')}');
                        }
                      },
                      child: Center(
                          child: Text(
                        "${DemoLocalizations.of(context).trans('send')}",
                        style:
                            TextStyle(color: Colors.white, fontFamily: 'Cairo'),
                      ))),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
