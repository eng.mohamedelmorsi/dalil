import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/Models/Market/searchMarket.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'Shop_Name.dart';

class SearchListExample extends StatefulWidget {
  final double lat;
  final double lng;

  const SearchListExample({Key key, this.lat, this.lng}) : super(key: key);
  
  @override
  _SearchListExampleState createState() => new _SearchListExampleState();
}

class _SearchListExampleState extends State<SearchListExample> {
  AutoCompleteTextField autoCompleteTextField;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  NetworkUtil _util = NetworkUtil();

  int itemCount = 0;
  SearchMarket searchMarket = SearchMarket();
  List<Datum> searchList = List();

  void showLoadingDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 10,
            content: Container(
              height: 150,
              width: 150,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text(
                      "${DemoLocalizations.of(context).trans('wait')}",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
  

  String key;

  void _submitForm() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    print(key);
    FormData _formData = FormData.from({
      'search_term': key,
      "lat":widget.lat,
      "long":widget.lng
    });
    FormData _headers = FormData.from({'Content-Type': 'application/json'});
    showLoadingDialog();

    Response response =
        await _util.post('marketSearch', body: _formData, headers: _headers);
      Navigator.of(context);

    if (response.statusCode == 200) {
                         Navigator.pop(context);
  
      setState(() {


        searchMarket = SearchMarket.fromJson(json.decode(response.toString()));
        itemCount = searchMarket.data.length;
        searchList.addAll(searchMarket.data);
      });
    } else {
      setState(() {
        searchList.removeWhere((item) {
          item.id;
        });
      });
    }
  }

  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: new Scaffold(
          key: _scaffoldKey,
          appBar: buildAppBar(
            context,
          ),
          body: 
          new Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: _searchItems(),
          )),
    );
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: InkWell(
            onTap: () {},
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: "Cairo",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _searchItems() {
    return searchList.length > 0
        ? ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: searchList.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () async {
                  var connectivityResult =
                      await Connectivity().checkConnectivity();
                  if (connectivityResult == ConnectivityResult.mobile ||
                      connectivityResult == ConnectivityResult.wifi) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShopName(
                                  phone: searchList[index].phone,
                                  distance: searchList[index].distance,
                                  rate: searchList[index].rate,
                                  id: searchList[index].id,
                                  name: searchList[index].name,
                                  start: searchList[index].startTime,
                                  end: searchList[index].endTime,
                                  image: searchList[index].image,
                                  lat: double.parse( searchList[index].latitude),
                                  long: double.parse( searchList[index].longitude),
                                )));
                  } else {
                    showInSnackBar(
                        '${DemoLocalizations.of(context).trans('no_internet')}');
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 10, left: 10, top: 10),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(.5)),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 100,
                              width: 100,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(right: 10, left: 10),
                                child: Container(
                                  width: 90,
                                  height: 130,
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/logo.png',
                                    image: searchList[index].image,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              height: 130,
                              width: 200,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 10),
                                          child: Text(
                                            searchList[index].name,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                right: 3, left: 3),
                                            child: StarRating(
                                              size: 17.0,
                                              rating: searchList[index].rate,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              borderColor: Theme.of(context)
                                                  .primaryColor,
                                              starCount: 5,
                                            )),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                              'assets/time.png',
                                            ),
                                            height: 15,
                                          ),
                                          Text(
                                            " ${DemoLocalizations.of(context).trans('fro')} " +
                                                searchList[index].startTime +
                                                " " +
                                                " ${DemoLocalizations.of(context).trans('t')}  " +
                                                searchList[index].endTime,
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                              'assets/location.png',
                                            ),
                                            height: 17,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            '${DemoLocalizations.of(context).trans('distance')}  "${searchList[index].distance}" ${DemoLocalizations.of(context).trans('km')}',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        )),
                  ),
                ),
              );
            },
          )
        : _buildNonDataView();
  }

  Widget _buildNonDataView() {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(top: 150),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.search,
              color: Colors.black,
              size: 40,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              '${DemoLocalizations.of(context).trans('no_data')}',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return new AppBar(
      automaticallyImplyLeading: false,
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back_ios),
      ),
      title: Form(
        key: _formKey,
        child: Center(
          child: TextField(
            style: TextStyle(color: Theme.of(context).primaryColor),
            onSubmitted: (val){
              setState(() {
                key = val;
               
      
                 setState(() {
                    _submitForm();
                  searchList.removeWhere((item) {
                    item.id;
                  });
              });
         
              });
            },
            decoration: InputDecoration(
                hintText: '${DemoLocalizations.of(context).trans('search')}',
                filled: true,
                contentPadding:
                    EdgeInsets.only(left: 10, bottom: 10, right: 10, top: 10),
               
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).primaryColor, width: 1.0),
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                  borderSide: BorderSide(
                      color: Theme.of(context).primaryColor, width: 1.0),
                ),
                fillColor: Colors.white,
                hintStyle: TextStyle(color: Theme.of(context).primaryColor)),
          ),
        ),
      ),
    );
  }
}
