import 'package:delivery_guide/EndDrawer/End_Drawer.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Scoped_Model/network_util.dart';
import 'Models/abouts.dart';
import 'dart:convert';

class Terms extends StatefulWidget {
  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  NetworkUtil _util = NetworkUtil();
  GetAllTerms about = GetAllTerms();
  _getAbout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> headers = {
      'lang': prefs.getString("lang"),
    };
    Response response = await _util.get('getAllTerms', headers: headers);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        about = GetAllTerms.fromJson(json.decode(response.toString()));
      });
    } else {}
  }

  @override
  void initState() {
    _getAbout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
            ? TextDirection.ltr
            : TextDirection.rtl,
        child: Scaffold(
          drawer: SizedBox(
               width: MediaQuery.of(context).size.width *.55,
            height: MediaQuery.of(context).size.height,
                      child: Drawer(
              child: EndDrawer().endDrawer(
                  Theme.of(context).primaryColor, context, '/register'),
            ),
          ),
          appBar: AppBar(
            title: Text(' ${DemoLocalizations.of(context).trans('Terms')}',
                style: TextStyle(
                  color: Colors.white,
                )),
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
          ),
          body: ListView(
            children: <Widget>[_image(), _displayedPage()],
          ),
        ));
  }

  Widget _image() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/logo.png'))),
      ),
    );
  }

  Widget _text() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: about.data.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(top: 30, left: 30, right: 30),
          child: Column(
            children: <Widget>[
              Container(
                child: Text(about.data[index].name,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 17)),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: Text(
                  about.data[index].description,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Widget _buildLoadingView() {
    return Padding(
      padding: const EdgeInsets.only(top: 200),
      child: Center(child: CircularProgressIndicator(
        backgroundColor:  Theme.of(context).primaryColor,
       
       
      )),
    );
  }

  Widget _displayedPage() {
    if (about.data == null) {
      return _buildLoadingView();
    } else {
      return _text();
    }
  }
}
