import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:delivery_guide/EndDrawer/End_Drawer.dart';
import 'package:delivery_guide/Items_Screen.dart';
import 'package:delivery_guide/Models/getAllCategories.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/Search_Screen.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePageScreen extends StatefulWidget {
  final double lat;
  final double lng;
  final String name;
  const HomePageScreen({Key key, this.lat, this.lng, this.name})
      : super(key: key);
  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}


class _HomePageScreenState extends State<HomePageScreen> {
  var location = Location();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var connectivityResult;

  double currentLat;
  double currentLong;

  bool _isLoading = false;
  NetworkUtil _util = NetworkUtil();
  GetAllCategories getAllCategories = GetAllCategories();
  _getAllCategories() async {
    SharedPreferences _prfes = await SharedPreferences.getInstance();
    setState(() {
      _isLoading = true;
    });
    FormData _headers = FormData.from({
      "lang": _prfes.getString('lang'),
    });
    Response response = await _util.get('getAllCategories', headers: _headers);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        getAllCategories =
            GetAllCategories.fromJson(json.decode(response.toString()));
        _isLoading = false;
      });
    } else {}
  }

  connect() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  @override
  void initState() {
     _getAllCategories();
    connect();
    LocationPermissions().requestPermissions().then((PermissionStatus status) {
      if (status == PermissionStatus.granted) {
        location.getLocation().then((LocationData myLocation) {
          setState(() {
            currentLat = myLocation.latitude; 
            currentLong = myLocation.longitude;
            print(currentLat.toString() + ' =>>>>>>> currentLat      ');
            print(currentLong.toString() + ' =>>>>>>>>> currentLong ');
          });
        });
      } else {}
    });
    _getAllCategories();

    super.initState();
  }

  void showInSnackBar(String title) {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 30,
          child: Column(
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontFamily: "Cairo",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
          key: _scaffoldKey,
          drawer:SizedBox(
            width: MediaQuery.of(context).size.width *.55,
            height: MediaQuery.of(context).size.height,
            child:  Drawer(
            child: EndDrawer().endDrawer(
                Theme.of(context).primaryColor, context, '/register'),
          ),
          ),
          appBar: AppBar(
            title: Text('${DemoLocalizations.of(context).trans('home_page')}'),
            centerTitle: true,
            actions: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchListExample(
                            lat: currentLat,
                            lng: currentLong,
                          )));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10,right: 10),
                  child: InkWell(
                      child: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 35,
                  )),
                ),
              ),
            ],
          ),
          body: _isLoading
              ? Center(
                  child: CupertinoActivityIndicator(
                    animating: true,
                    radius: 15,
                  ),
                )
              : ListView(
                primary: true,
                  shrinkWrap: true,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Container(
                        height: 110,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          image: AssetImage(
                            'assets/banner.png',
                          ),
                        )),
                      ),
                    ),
                    _count(),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                )),
    );
  }

  Widget _buildNonDataView() {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(top: 150),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Text(
              ' ${DemoLocalizations.of(context).trans('no_data')}',
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
          ],
        ),
      ),
    );
  }

  Widget _count() {
    return getAllCategories.data.length > 0
        ? Padding(
            padding: const EdgeInsets.only(right: 30, left: 30),
            child: SizedBox(
              height: MediaQuery.of(context).size.height - 210,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                primary: false,
                crossAxisCount: 2,
                padding: EdgeInsets.only(top: 10),
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                children: List.generate(getAllCategories.data.length, (index) {
                  return new GridTile(
                      child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(.2))),
                    child: InkWell(
                      onTap: () async {
                        var connectivityResult =
                            await Connectivity().checkConnectivity();
                        if (connectivityResult == ConnectivityResult.mobile ||
                            connectivityResult == ConnectivityResult.wifi) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ItemsScreen(
                                        lat: currentLat,
                                        lng: currentLong,
                                        cateId: getAllCategories.data[index].id,
                                        name: getAllCategories.data[index].name,
                                      )));
                        } else {
                          showInSnackBar(
                              '${DemoLocalizations.of(context).trans('no_internet')}');
                        }
                      },
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/logo.png',
                              image: getAllCategories.data[index].image,
                              height: 90,
                            ),
                          ),
                          Center(
                            child: new Text(
                              getAllCategories.data[index].name,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ));
                }),
              ),
            ),
          )
        : _buildNonDataView();
  }
}
