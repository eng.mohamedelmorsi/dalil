import 'package:delivery_guide/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'localization/DemoLocalization.dart';

class Mada extends StatefulWidget {
  @override
  _MadaState createState() => _MadaState();
}

class _MadaState extends State<Mada> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            ' ${DemoLocalizations.of(context).trans('pament')}',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: ListView(
          children: <Widget>[
            _text(),
            numberSub(),
            numberCard(),
            _cvc(),
            _pay()
          ],
        ),
      ),
    );
  }

  Widget _text() {
    return Padding(
      padding: EdgeInsets.only(top: 30, right: 20, left: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            '${DemoLocalizations.of(context).trans('insert')}',
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          Image(
            image: AssetImage('assets/mada.png'),
            height: 20,
          )
        ],
      ),
    );
  }

  Widget _cvc() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 20, top: 20, left: 20),
          child: Container(
            height: 50,
            width: 140,
            child: TextFormField(
              onSaved: (String value) {
                setState(() {
                  // _formData['prideName'] = value;
                });
              },
              // autovalidate: _autoValidate,
              validator: (String value) {
                if (value.isEmpty || value.split(" ").length < 4) {
                  return '  mm/yyأدخل رقم  ';
                } else
                  return null;
              },
              keyboardType: TextInputType.phone,

              // controller: _prideNameController,
              style: TextStyle(
                  fontFamily: " Cairo",
                  color: Theme.of(context).primaryColor,
                  fontSize: 15),
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                    fontFamily: " Cairo",
                    color: Theme.of(context).primaryColor,
                    fontSize: 13,
                  ),
                  contentPadding:
                      EdgeInsets.only(left: 10, top: 18, bottom: 18, right: 10),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 1.0),
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 1.0),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  labelText: 'mm/yy',
                  labelStyle:
                      TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20, top: 20, left: 20),
          child: Container(
            height: 50,
            width: 140,
            child: TextFormField(
              onSaved: (String value) {
                setState(() {
                  // _formData['prideName'] = value;
                });
              },
              // autovalidate: _autoValidate,
              validator: (String value) {
                if (value.isEmpty || value.split(" ").length < 4) {
                  return '  صحيح CVC أدخل رقم  ';
                } else
                  return null;
              },
              keyboardType: TextInputType.phone,

              // controller: _prideNameController,
              style: TextStyle(
                  fontFamily: " Cairo",
                  color: Theme.of(context).primaryColor,
                  fontSize: 15),
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                    fontFamily: " Cairo",
                    color: Theme.of(context).primaryColor,
                    fontSize: 13,
                  ),
                  contentPadding:
                      EdgeInsets.only(left: 10, top: 18, bottom: 18, right: 10),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 1.0),
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 1.0),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  labelText: 'CVC',
                  labelStyle:
                      TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
            ),
          ),
        ),
      ],
    );
  }

  Widget numberSub() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            // _formData['prideName'] = value;
          });
        },
        // autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.split(" ").length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,

        // controller: _prideNameController,
        style: TextStyle(
            fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 18, bottom: 18, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('subs')} ',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }

  Widget numberCard() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            // _formData['prideName'] = value;
          });
        },
        // autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty || value.split(" ").length < 4) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')} ';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        // controller: _prideNameController,
        style: TextStyle(
            fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 18, bottom: 18, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('card')} ',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }

  Widget _pay() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 200),
          child: InkWell(
            onTap: () {
              //  Navigator.push(context,
              //   MaterialPageRoute(builder: (context) => HomePageScreen()));
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  ' ${DemoLocalizations.of(context).trans('pament')}',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }
}
