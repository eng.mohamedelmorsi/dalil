import 'package:delivery_guide/utils/color.dart';
import 'package:flutter/material.dart';

import 'localization/DemoLocalization.dart';

class SerialNumber extends StatefulWidget {
  @override
  _SerialNumberState createState() => _SerialNumberState();
}

class _SerialNumberState extends State<SerialNumber> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(' ${DemoLocalizations.of(context).trans('pament')}',
              style: TextStyle(
                color: Colors.white,
              )),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: ListView(
          children: <Widget>[_text(), numberSub(), serialNu(), _pay()],
        ),
      ),
    );
  }

  Widget serialNu() {
    return Container(
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: Theme.of(context).primaryColor)),
        margin: EdgeInsets.only(right: 20, left: 20, top: 20),
        height: 50.0,
        child: Material(
          elevation: 1.0,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30.0))),
          child: TextFormField(
            style: TextStyle(color: Theme.of(context).primaryColor),
            validator: (String value) {
              if (value.isEmpty || value.length < 6) {
                return '${DemoLocalizations.of(context).trans('vaild_phone')} ';
              }
            },
            onSaved: (String value) {},
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(right: 20, top: 10, left: 10),
              border: InputBorder.none,
              hintText: '${DemoLocalizations.of(context).trans('serial')}',
              hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
            ),
          ),
        ));
  }

  Widget _pay() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 250),
          child: InkWell(
            onTap: () {
              //  Navigator.push(context,
              //   MaterialPageRoute(builder: (context) => HomePageScreen()));
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  ' ${DemoLocalizations.of(context).trans('active')}',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }

  Widget _text() {
    return Padding(
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '${DemoLocalizations.of(context).trans('insert')}',
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 19,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget numberSub() {
    return Container(
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: Theme.of(context).primaryColor)),
        margin: EdgeInsets.only(right: 20, left: 20, top: 20),
        height: 50.0,
        child: Material(
          elevation: 1.0,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30.0))),
          child: TextFormField(
            style: TextStyle(color: Theme.of(context).primaryColor),
            validator: (String value) {
              if (value.isEmpty || value.length < 6) {
                return '${DemoLocalizations.of(context).trans('vaild_phone')} ';
              }
            },
            onSaved: (String value) {},
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(right: 20, top: 10, left: 10),
              border: InputBorder.none,
              hintText: '${DemoLocalizations.of(context).trans('subs')} ',
              hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
            ),
          ),
        ));
  }
}
