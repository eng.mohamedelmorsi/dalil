import 'dart:convert';
import 'package:delivery_guide/Models/geAllCities.dart';
import 'package:delivery_guide/Payment.dart';
import 'package:delivery_guide/Scoped_Model/network_util.dart';
import 'package:delivery_guide/localization/DemoLocalization.dart';
import 'package:delivery_guide/utils/color.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Models/Market/setNewClass.dart';





class Raking extends StatefulWidget {
  @override
  _RakingState createState() => _RakingState();
}

class _RakingState extends State<Raking> {
  SetNewClassification setNewClassification;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  NetworkUtil _util = NetworkUtil();

  String shop_field, phone, shop_name, state;
  int city_id;

  @override
  void initState() {
    _getAllCities();
    super.initState();
  }

  _submit(BuildContext context) async {
    
    FormState formState = _formKey.currentState;

    void showLoadingDialog() {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              elevation: 10,
              content: Container(
                height: 150,
                width: 150,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Text(
                        "${DemoLocalizations.of(context).trans('wait')}",
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    }

    if (formState.validate()) {
      formState.save();

      FormData _formData = FormData.from({
        "shop_field": shop_field,
        "shop_name": shop_name,
        "phone": phone,
        "city_id": cit.id,
        "state": state,
      });

      FormData _headers = FormData.from({
        "Content-Type": "application/json",
      });

      showLoadingDialog();
      _util
          .post('setNewClassification', body: _formData, headers: _headers)
          .then((result) {
        Navigator.pop(context);
        print("---------> $result");
        if (result.statusCode == 200) {
          setNewClassification = SetNewClassification.fromJson(result.data);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Payment()));
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                print(result.data['message']);
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  title: Text(
                    result.data['message'],
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).primaryColor),
                  ),
                  actions: <Widget>[
                    Center(
                      child: FlatButton(
                        child: Text(
                            '${DemoLocalizations.of(context).trans('es')}'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    )
                  ],
                );
              });
        }
      }).catchError((err) {
        print("333333333333333333333333#########" + err.toString());
      });
    }
  }

  bool _isLoading = false;
  GetAllCities getAllCities = GetAllCities();
  _getAllCities() async {
    SharedPreferences _prfes = await SharedPreferences.getInstance();

    setState(() {
      _isLoading = true;
    });
    FormData _headers = FormData.from({
      "lang": _prfes.getString('lang'),
    });
    Response response = await _util.get('getAllCities', headers: _headers);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      setState(() {
        getAllCities = GetAllCities.fromJson(json.decode(response.toString()));
        _isLoading = false;
      });
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: DemoLocalizations.of(context).locale.languageCode == 'en'
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            '${DemoLocalizations.of(context).trans('other')}',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  shopFiled(),
                  shopName(),
                  textFiledPhone(),
                  _cities(),
                  _street(),
                ],
              ),
            ),
            _register()
          ],
        ),
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment(0.0, 0.0),
      child: Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 130),
          child: InkWell(
            onTap: () {
              _submit(context);
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,

                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5, 0.9],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(getColorHexFromStr('#E2548E')),
                    Color(
                      getColorHexFromStr("#F5AFBA"),
                    ),
                  ],
                ),
              ),
              child: Center(
                child: Text(
                  '${DemoLocalizations.of(context).trans('register')}',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
    );
  }

  Datum cit;

  void _openCitiesBottomSheet(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: (MediaQuery.of(context).size.height / 2) + 100,
            child: CupertinoActionSheet(
              cancelButton: CupertinoButton(
                child: Text(
                  '${DemoLocalizations.of(context).trans('no')}',
                  style: TextStyle(
                      fontFamily: "Cairo",
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: getAllCities.data
                  .map(
                    (city) => CupertinoButton(
                      child: Center(
                        child: Text(
                          city.name ?? "",
                          style: TextStyle(
                              fontFamily: "Cairo",
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        setState(() {
                          cit = city;
                        });
                      },
                    ),
                  )
                  .toList(),
            ),
          );
        });
  }

  Widget _cities() {
    return InkWell(
      onTap: () {
        _openCitiesBottomSheet(context);
      },
      child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: Theme.of(context).primaryColor)),
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          height: 50.0,
          child: Material(
            elevation: 1.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 20, left: 9),
                  child: Image(
                    image: AssetImage('assets/location.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Text(
                    cit == null
                        ? "${DemoLocalizations.of(context).trans('Citi')}"
                        : cit.name ?? "",
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                ),
                Icon(Icons.arrow_drop_down, color: Colors.grey),
              ],
            ),
          )),
    );
  }

  Widget _street() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            state = value;
          });
        },
        validator: (String value) {
          if (value.isEmpty) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')}';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 25.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/location.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '  ${DemoLocalizations.of(context).trans('street')}   ',
            labelStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget textFiledPhone() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            phone = value;
          });
        },
        // autovalidate: _autoValidate,
        validator: (String value) {
          if (value.isEmpty) {
            return '${DemoLocalizations.of(context).trans('vaild_phone')}';
          } else
            return null;
        },
        keyboardType: TextInputType.phone,
        // controller: _prideNameController,
        style: TextStyle(
            fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 15),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 18, bottom: 18, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/phone.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('phone')}',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }

  Widget shopFiled() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            shop_field = value;
          });
        },
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')} ';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        style: TextStyle(
            // fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 14),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              // fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/user.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: 'مجال المحل',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }

  Widget shopName() {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
      child: TextFormField(
        onSaved: (String value) {
          setState(() {
            shop_name = value;
          });
        },
        validator: (String value) {
          if (value.isEmpty || value.length < 4) {
            return '${DemoLocalizations.of(context).trans('valid_shopname')} ';
          } else
            return null;
        },
        keyboardType: TextInputType.text,
        style: TextStyle(
            // fontFamily: " Cairo",
            color: Theme.of(context).primaryColor,
            fontSize: 14),
        decoration: InputDecoration(
            errorStyle: TextStyle(
              // fontFamily: " Cairo",
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
            contentPadding:
                EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
            ),
            prefixIcon: Padding(
                padding: EdgeInsets.all(15),
                child: Image(
                    width: 22.0,
                    height: 22.0,
                    color: Theme.of(context).primaryColor,
                    image: AssetImage('assets/user.png'))),
            filled: true,
            fillColor: Colors.white,
            labelText: '${DemoLocalizations.of(context).trans('shop_name')}',
            labelStyle: TextStyle(color: Colors.grey, fontFamily: 'Cairo')),
      ),
    );
  }
}
